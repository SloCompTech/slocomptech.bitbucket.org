
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/*
  Zemljevid
*/
var mapa; // Zemljevid bolnišnic
var markerji = []; // Markerji na zemljevidu
var izbranaTocka = null; // Izbrana tocka na zemljevidu
var mapRouter = null; // Prikazovalnik poti

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

/*
  Vnos podatkov
*/
var gTrenutniZavihek = 1;
var gIzbraniEHRId = null; // EHR ID za katerega prikazujemo podatke 
var gDrzave = []; // Seznam drzav
var gKraji = []; // Seznam krajev
var gPacienti = [ // Predlogi že vnešenih uporabnikov v EHR shape
  { ehrId: "441dfbc6-d8a8-4432-bf68-660a6182641a", name: "Miha Novak" },
  { ehrId: "687d1f4e-c038-425e-a3cf-3e525eed8995", name: "Ana Jazbec" },
  { ehrId: "b8a774b8-e9d4-494b-bee3-4bbc8918d069", name: "Timotej Kotnik" }
]; 

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  let novKarton;
  let noveMeritve = [];

  if (stPacienta == 1) {
    novKarton = {
      firstNames: "Miha",
      lastNames: "Novak",
      dateOfBirth: "1980-04-01",
      gender: "MALE",
      address: {
        address: "Ljubljana, Slovenia"
      }
    };
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2012-12-12T13:00:00Z",
      data: {
        height: 150,
        weight: 56,
        temperature: 36.5,
        systolic: 100,
        diastolic: 100,
        oximetry: 95
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2013-12-12T17:45:00Z",
      data: {
        height: 156,
        weight: 60,
        temperature: 36.4,
        systolic: 98,
        diastolic: 98,
        oximetry: 96
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2014-12-12T15:22:00Z",
      data: {
        height: 159,
        weight: 62,
        temperature: 36.6,
        systolic: 99,
        diastolic: 99,
        oximetry: 90
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2015-12-12T08:14:00Z",
      data: {
        height: 161,
        weight: 61,
        temperature: 36.5,
        systolic: 96,
        diastolic: 96,
        oximetry: 97
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2016-12-12T15:36:00Z",
      data: {
        height: 164,
        weight: 65,
        temperature: 36.5,
        systolic: 102,
        diastolic: 102,
        oximetry: 94
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2017-12-12T17:18:00Z",
      data: {
        height: 167,
        weight: 66,
        temperature: 36.3,
        systolic: 100,
        diastolic: 100,
        oximetry: 96
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2018-12-12T18:11:33Z",
      data: {
        height: 170,
        weight: 70,
        temperature: 36.5,
        systolic: 101,
        diastolic: 101,
        oximetry: 95
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2019-01-12T12:51:47Z",
      data: {
        height: 170,
        weight: 68,
        temperature: 36.5,
        systolic: 98,
        diastolic: 98,
        oximetry: 94
      }
    });
  }
  else if (stPacienta == 2) {
     novKarton = {
      firstNames: "Ana",
      lastNames: "Jazbec",
      dateOfBirth: "1999-11-04",
      gender: "FEMALE",
      address: {
        address: "Logatec, Slovenia"
      }
    };
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2008-01-12T11:00:00Z",
      data: {
        height: 110,
        weight: 40,
        temperature: 36.5,
        systolic: 95,
        diastolic: 95,
        oximetry: 94
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2008-02-12T12:35:56Z",
      data: {
        height: 110,
        weight: 40,
        temperature: 36.3,
        systolic: 96,
        diastolic: 98,
        oximetry: 96
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2008-03-12T08:18:37Z",
      data: {
        height: 111,
        weight: 40,
        temperature: 36.6,
        systolic: 97,
        diastolic: 93,
        oximetry: 94
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2008-04-12T13:44:11Z",
      data: {
        height: 111,
        weight: 40,
        temperature: 36.4,
        systolic: 98,
        diastolic: 95,
        oximetry: 97
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2008-05-12T17:42:51Z",
      data: {
        height: 111,
        weight: 40,
        temperature: 36.5,
        systolic: 94,
        diastolic: 94,
        oximetry: 93
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2008-06-12T18:11:36Z",
      data: {
        height: 111,
        weight: 40,
        temperature: 36.3,
        systolic: 99,
        diastolic: 99,
        oximetry: 98
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2008-07-12T18:32:56Z",
      data: {
        height: 111,
        weight: 40,
        temperature: 36.4,
        systolic: 96,
        diastolic: 98,
        oximetry: 96
      }
    });
  }
  else if (stPacienta == 3) {
    novKarton = {
      firstNames: "Timotej",
      lastNames: "Kotnik",
      dateOfBirth: "1994-03-28",
      gender: "MALE",
      address: {
        address: "Trzin, Slovenia"
      }
    };
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2010-01-21T11:33:00Z",
      data: {
        height: 154,
        weight: 52,
        temperature: 36.5,
        systolic: 91,
        diastolic: 99,
        oximetry: 98
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2011-08-29T18:17:44Z",
      data: {
        height: 151,
        weight: 53,
        temperature: 36.3,
        systolic: 98,
        diastolic: 96,
        oximetry: 95
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2013-01-20T08:11:34Z",
      data: {
        height: 167,
        weight: 61,
        temperature: 36.6,
        systolic: 102,
        diastolic: 95,
        oximetry: 96
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2015-07-18T09:22:41Z",
      data: {
        height: 170,
        weight: 65,
        temperature: 36.6,
        systolic: 100,
        diastolic: 98,
        oximetry: 94
      }
    });
    noveMeritve.push({
      country: "SI",
      merilec: "Micka Novak",
      datumInUra: "2018-06-03T14:03:41Z",
      data: {
        height: 171,
        weight: 68,
        temperature: 36.6,
        systolic: 99,
        diastolic: 103,
        oximetry: 95
      }
    });
  }
  
  if (novKarton != null) {
    let ehrId = ustvariZdravstveniKarton(novKarton, function (err, ehrId) {
      if (err) {
        console.error(err);
        toastr.error(err.message, "Napaka");
        return;
      }
      toastr.success(ehrId,"Karton ustvarjen");
      $("#alertbox").append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Ustvarjen novi karton <strong>' + ehrId + '</strong></div>');
      gPacienti.push({ ehrId: ehrId, name: novKarton.firstNames + " " +  novKarton.lastNames }); // Dodaj k predlogom
      
      posodobiPrikazSeznamaPacientov();
      
      noveMeritve.forEach((m) => {
        ustvariMeritev(ehrId, m.country, m.datumInUra, m.merilec, m.data, function (err, data){
          if (err) {
            console.error(err);
            return;
          }
        });
      });
    });
    return ehrId;
  }
  return null;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
/*
  Event handlerji
*/
/**
 * Ob kliku, da spremenimo zavihek
 */
function onNavbarLinkClick(e) {
  let navId = $(this).parent("ul").attr("navId");
  let optId = $(this).attr("optId");
  let disabled = $(this).hasClass("disabled");
  
  // Don't react on disabled items
  if (disabled)
    return;
  
  // TODO: Other reaction (show different tabs)
  if (navId == 1)
    spremeniZavihek(optId);
}

/**
 * Prikažemo drug zavihek
 */
function spremeniZavihek(zavihek) {
  if (!zavihek || !$("#zavihki").length)
    return;
  
  // Preveri, če je slučajno opcija za zavihek onemogočena
  if ($("#navigacija li[optid=" + zavihek + "]").hasClass("disabled"))
    return;
  
  gTrenutniZavihek = zavihek;
  
  // Spremeni aktivni zavihek v navihaciji
  // Clear active class on other elements of nav
  $("#navigacija").children("li").each(function (){
    $(this).removeClass("active");
  });
  
  // Add class active to clicked element
  $("#navigacija li[optid=" + zavihek + "]").addClass("active");
  //console.log(navId + ":" + optId);
  
  // Prikaži izbran zavihek
  $("#zavihki").children("div").each(function(){
    if ($(this).attr("zavihekId") == zavihek) {
      if ($(this).hasClass("hidden"))
        $(this).removeClass("hidden"); // Show selected tab
    } else {
      if (!$(this).hasClass("hidden"))
        $(this).addClass("hidden"); // Hidde other tabs
    }
  });
  
  // Load data for tab
  if (zavihek == 3) { // Vnos podatkov
    if (gKraji.length == 0) {
      pridobiSeznamKrajev(function (err, kraji) {
        if (err)
          return;
        
        if (Array.isArray(kraji)) {
          kraji.forEach(function(item) { gKraji.push({ label: item.kraj, value: item.kraj }); }); // Dodaj kraj na seznam predlaganih
          $("#uk_kraj").autocomplete({
  					source: gKraji,
  					minLength: 3
  				});
  				$("#uk_kraj").autocomplete(($("#uk_kraj").val() == "Slovenia")?"enable":"disable"); // Omogoči/Onemogoči auutocomplete
        }
      });
      // Naloži seznam držav, če še ni
    }
    if (gDrzave.length == 0) {
      pridobiSeznamDrzav(function (err, data) {
        if (err)
          return;
        
        if (Array.isArray(data)) {
          // Dodaj drzave na seznam
          data.forEach(function(item) { gDrzave.push({ label: item.Country, value: item.Country, code: item.ISO }); }); // Dodaj drzavo na seznam predlaganih
          
          /*
            Dodaj autocomplete
            @see http://api.jqueryui.com/autocomplete/#method-disable
          */
          $("#uk_drzava").autocomplete({
  					source: gDrzave,
  					minLength: 3,
  					select: function(event, ui) {
  					 $("#uk_kraj").autocomplete((ui.item.label == "Slovenia")?"enable":"disable"); // Omogoči/Onemogoči auutocomplete
  					}
  				});
  				$("#vm_drzava").autocomplete({
  					source: gDrzave,
  					minLength: 3,
  					select: function(event, ui) {
  					 $("#vm_drzavaCode").val(ui.item.code); // Nastavi kodo drzave
  					}
  				});
        }
      });
    }
    
    // Če je izbran nek uporabnik, avtomatično zaponlimo polja ehrId
    if (gIzbraniEHRId && gIzbraniEHRId.trim().length > 0) {
      $("#pk_ehrId").val(gIzbraniEHRId);
      $("#vm_ehrId").val(gIzbraniEHRId);
      $("#pm_ehrId").val(gIzbraniEHRId);
      pocistiPrikazanePodatkePacient(); // Pocisti tabele na zavihu 3
    }
  }
  
  if (zavihek == 1) { // Prikaz seznama pacientov
    posodobiPrikazSeznamaPacientov();
  }
  
  if (zavihek == 2) { // Prikaz podatkov za pacienta
    // Počisti prejšnje stanje
    $("#sw_pacient").html("");
    
    preberiZdravstveniKarton(gIzbraniEHRId, function (err, data) {
      if (err) {
        console.log(err);
        toastr.error(err.message, "Napaka");
        return;
      }
      
      // Dodaj pacienta na seznam pacientov
      let found = false;
      for(let i = 0; i < gPacienti.length; i++)
        if (gPacienti[i].ehrId == gIzbraniEHRId) {
          found = true;
          break;
        }
      if (!found) { // Predloga še ni na seznamu zato ga dodaj
        gPacienti.push({ ehrId: gIzbraniEHRId, name: data.firstNames + " " +  data.lastNames }); // Dodaj k predlogom
        posodobiPrikazSeznamaPacientov();
      }
      
      // Prikaz podatkov o pacientu
      $("#sw_pacient").html(data.firstNames + " " + data.lastNames + " (" + ((data.gender == "MALE")?"M, ":((data.gender == "FEMALE")?"Ž, ":"")) + data.dateOfBirth + ((data.address && data.address.address)?", " + data.address.address:"") + ")");
      
      // Prikazi podatke o pacientu
      prikaziPodatkePacienta();
    });
    
  }
}

/**
 * Prikaži podatke pacienta v 2 zavihku
 */
function prikaziPodatkePacienta() {
  // Počisti pred seboj
  $("#sw_space").html("");
  $("svg").html("");
  
  // Naloži prikaz
  let tip = $("#sw_seznamTipov").val();
  let limit = $("#sw_steviloMeritev").val();
  preberiMeritve(gIzbraniEHRId, tip, {
    limit: limit
  }, function(err, data){
    if (err) {
      console.error(err);
      toastr.error(err.message, "Napaka");
      return;
    }
    
    let graf = null; // Podatki o grafu
    
    // Izriši tabelo meritev
    let table = '<table class="table"><thead><tr><th>Datum</th>';
    if (tip == "height") {
      table += '<th>Višina</th>';
       graf = {
        name: "Meritve telesne višine",
        yLegend: "Višina (cm)",
        yDomain: [0, 220],
        data: data.map((e) => {
          // Pretvori v obliko za prikaz
          return {
            time: e.time,
            value: e.height,
            color: "blue",
            textColor: "white"
          }
        })
      };
    } else if (tip == "weight") {
      table += '<th>Teža</th>';
       graf = {
        name: "Meritve telesne teže",
        yLegend: "Teža (kg)",
        yDomain: [0, 150],
        data: data.map((e) => {
          // Pretvori v obliko za prikaz
          return {
            time: e.time,
            value: e.weight,
            color: "blue",
            textColor: "white"
          }
        })
      };
    } else if (tip == "temperature") {
      table += '<th>Temperatura</th>';
      graf = {
        name: "Meritve telesne temperature",
        yLegend: "Temperatura (°C)",
        yDomain: [30, 50],
        data: data.map((e) => {
          // Pretvori v obliko za prikaz
          return {
            time: e.time,
            value: e.temperature,
            color: "orange",
            textColor: "white"
          }
        })
      };
      
    } else if (tip == "pressure") {
      table += '<th>Diastolic</th>';
      table += '<th>Systolic</th>';
      graf = {
        name: "Krvni tlak (Zelen - distolični, oranžen - sistolični)",
        yLegend: "Tlak (mm Hg)",
        yDomain: [70, 200],
        data: data.map((e) => {
          // Pretvori v obliko za prikaz
          return {
            time: e.time,
            value: [e.diastolic, e.systolic],
            color: ["green","orange"],
            textColor: "white"
          }
        })
      };
    } else if (tip == "spO2") {
      table += '<th>Nasičenost krvi</th>';
       graf = {
        name: "Naiščenost kisika v krvi",
        yLegend: "Nasičenost (%)",
        yDomain: [0, 100],
        data: data.map((e) => {
          // Pretvori v obliko za prikaz
          return {
            time: e.time,
            value: e.spO2,
            color: "red",
            textColor: "white"
          }
        })
      };
    } 
    table += '</tr></thead><tbody>';
    if (Array.isArray(data)) {
      for (let i = 0; i < data.length; i++) {
        table += '<tr><td>' + data[i].time + "</td>";
        if (tip == "height") {
          table += '<td>' + data[i].height + ' ' + data[i].unit + '</td>';
        } else if (tip == "weight") {
          table += '<td>' + data[i].weight + ' ' + data[i].unit + '</td>';
        } else if (tip == "temperature") {
          table += '<td>' + data[i].temperature + ' ' + data[i].unit + '</td>';
        } else if (tip == "pressure") {
          table += '<td>' + data[i].diastolic + ' ' + data[i].unit + '</td>';
          table += '<td>' + data[i].systolic + ' ' + data[i].unit + '</td>';
        } else if (tip == "spO2") {
          table += '<td>' + data[i].spO2 + ' %</td>';
        }
        table += '</tr>';
      }
    }
    table += '</tbody></table>';
    $("#sw_space").append(table);
    toastr.success("Podatki uspešno prebrani");
    
    if (graf != null) { // Podatki za graf so nastavljeni
      // Pripravi podatke za graf
      const margin = { top: 60, right: 60, bottom: 60, left: 60 };
      const width = $("svg").parent().width() - (margin.left + margin.right);
      const height = $("svg").parent().height() - (margin.top + margin.bottom);
      const svg = d3.select('svg');
      const chart = svg.append('g')
        .attr('transform', `translate(${margin.left}, ${margin.top})`); // Odmik od izhodišča (0,0) je zgoraj levo
      // X skala
      const xScale = d3.scaleBand()
        .range([0, width])
        //.domain([new Date(data[0].time), new Date(data[data.length - 1].time)])
        .domain(graf.data.map((d) => d.time))
        .padding(0.2);
                       
      // Nariši X skalo
      chart.append('g')
        .attr('transform', `translate(0, ${height})`)
        .call(d3.axisBottom(xScale));
      
      // Legenda X
      svg.append('text')
        .attr('class', 'label')
        .attr('x', width / 2 + margin.left)
        .attr('y', height + margin.top * 1.7)
        .attr('text-anchor', 'middle')
        .text('Meritve (od najbolj nove do stare)');
      
      // Y skala   
      const yScale = d3.scaleLinear()
        .range([height, 0])
        .domain(graf.yDomain);
        
      chart.append('g')  // Nariši Y skalo
           .call(d3.axisLeft(yScale));
           
      // Legenda Y
      svg.append('text')
      .attr('x', -(height / 2) - margin.left)
      .attr('y', margin.top / 2.4)
      .attr('transform', 'rotate(-90)')
      .attr('text-anchor', 'middle')
      .text(graf.yLegend);
        
      // Ime grafa
      svg.append('text')
        .attr('x', width / 2 + margin.left)
        .attr('y', 40)
        .attr('text-anchor', 'middle')
        .text(graf.name);
        
      // Nariši
      const barGroups = chart.selectAll()
        .data(graf.data)
        .enter()
        .append('g');
      // Nariši stolpce
      if (graf.data.length > 0 && !Array.isArray(graf.data[0].value)) {
        barGroups.append('rect')
          .attr('x', (d, index, array) => xScale(d.time))
          .attr('y', (d) => yScale(d.value))
          .attr('height', (d) => height - yScale(d.value))
          .attr('width', xScale.bandwidth())
          .attr('fill',(d) => (d.color)?d.color:'blue'); // Barva stolpcev
        barGroups 
          .append('text')
          .attr('class', 'value')
          .attr('x', (d) => xScale(d.time) + xScale.bandwidth() / 2)
          .attr('y', (d) => ((yScale(d.value) + 20 < height)?yScale(d.value) + 20:Math.min(yScale(d.value),height - 5)))
          .attr('text-anchor', 'middle')
          .attr('fill',(d) => ((yScale(d.value) + 20 < height)?((d.textColor)?d.textColor:"white"):"black")) // Barva besedila na stolpcih
          .text((d) => `${d.value}` + ((d.valSuffix)?" " + d.valSuffix:""));
      } else if (graf.data.length > 0) {
        for (let i = 0; i < graf.data[i].value.length; i++) {
          let bnd = xScale.bandwidth()/graf.data[i].value.length;
          barGroups.append('rect')
            .attr('x', (d, index, array) => xScale(d.time) + bnd * i)
            .attr('y', (d) => yScale(d.value[i]))
            .attr('height', (d) => height - yScale(d.value[i]))
            .attr('width', bnd)
            .attr('fill',(d) => (Array.isArray(d.color) && d.color.length > 0 && i < d.color.length)?d.color[i]:'blue'); // Barva stolpcev
          barGroups 
            .append('text')
            .attr('class', 'value')
            .attr('x', (d) => xScale(d.time) + bnd * i + bnd / 2)
            .attr('y', (d) => ((yScale(d.value[i]) + 20 < height)?yScale(d.value[i]) + 20:Math.min(yScale(d.value[i]),height - 5)))
            .attr('text-anchor', 'middle')
            .attr('fill',(d) => ((yScale(d.value[i]) + 20 < height)?((d.textColor)?d.textColor:"white"):"black")) // Barva besedila na stolpcih
            .text((d) => `${d.value[i]}` + ((d.valSuffix)?" " + d.valSuffix:""));
        }
        
      }
      
    }
  });
  
  
}

/**
 * Ko smo prejeli zahtevo, da ustvarimo novo kartoteko
 */
function onUstvariZravstveniKarton(e) {
  ustvariZdravstveniKarton({
    firstNames: $("#uk_ime").val(),
    lastNames: $("#uk_priimek").val(),
    dateOfBirth: $("#uk_datumRojstva").val(),
    gender: $("#uk_spol").val(),
    address: (0 + $("#uk_kraj").val().length + $("#uk_drzava").val().length > 0)?{
      address: (($("#uk_kraj").val())?$("#uk_kraj").val() + ", ":"") + $("#uk_drzava").val()
    }:null
  }, function (err, ehrId) {
    if (err) {
      console.error(err);
      e.data.toastr.error(err.message, "Napaka");
      return;
    }
    e.data.toastr.success(ehrId,"Karton ustvarjen");
    $("#alertbox").append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Ustvarjen novi karton <strong>' + ehrId + '</strong></div>');
    gPacienti.push({ ehrId: ehrId, name: $("#uk_ime").val() + " " +  $("#uk_priimek").val() }); // Dodaj k predlogom
    
    // Počisti vrednosti za seboj
    $("#uk_ime").val("");
    $("#uk_priimek").val("");
    $("#uk_datumRojstva").val("");
    $("#uk_spol").val("");
    $("#uk_kraj").val("");
    $("#uk_drzava").val("");
    
    posodobiPrikazSeznamaPacientov();
  });
}

/**
 * Osveži vse prikazane sezname pacientov
 */
function posodobiPrikazSeznamaPacientov() {
  $("#pk_seznamPacientov").html("");
  $("#pk_seznamPacientov").append("<option></option>"); // Always start with empty option
  gPacienti.forEach(function(item){
    $("#pk_seznamPacientov").append("<option value=\"" + item.ehrId + "\">" + item.name + "</option>");
  }, this);
  
  $("#vm_seznamPacientov").html("");
  $("#vm_seznamPacientov").append("<option></option>"); // Always start with empty option
  gPacienti.forEach(function(item){
    $("#vm_seznamPacientov").append("<option value=\"" + item.ehrId + "\">" + item.name + "</option>");
  }, this);
  
  $("#pm_seznamPacientov").html("");
  $("#pm_seznamPacientov").append("<option></option>"); // Always start with empty option
  gPacienti.forEach(function(item){
    $("#pm_seznamPacientov").append("<option value=\"" + item.ehrId + "\">" + item.name + "</option>");
  }, this);
  
  // Tabela: Zavihek 1
  let table = $("div[zavihekId=1] table tbody");
  table.html(""); // Počisti tabelo
    
  for (let i = 0; i < gPacienti.length; i++) {
    table.append('<tr><td>' + gPacienti[i].name + '</td><td><button type="button" class="btn btn-info" action="view" ehrId="' + gPacienti[i].ehrId + '"><span class="glyphicon glyphicon-object-align-bottom"></span> Pregled</button> <button type="button" class="btn btn-success" action="edit" ehrId="' + gPacienti[i].ehrId + '"><span class="glyphicon glyphicon-pencil"></span> Uredi</button></td></tr>');
  }
  
  $("div[zavihekId=1] table tbody tr td button").on("click", function(e){
    let action = $(this).attr("action");
    if (action == "view") {
      onPrikaziPodatkePacienta(e);
    } else if (action == "edit") {
      onUrediPodatkePacienta(e);
    }
  });
}

/**
 * Počisti vse izpisane podatke
 */
function pocistiPrikazanePodatkePacient() {
  $("#pk_space").html("");
  $("#pm_space").html("");
}

/**
 * Ko smo prejeli zahtevo za branje zdravstevenga kartona
 */
function onPreberiZravstveniKarton(e) {
  $("#pk_space").html("");
  let ehrId = $("#pk_ehrId").val();
  
  if (!ehrId || ehrId.trim().length == 0) {
    e.data.toastr.error("Vnesi ehrId", "Napaka");
    return;
  }
  
  preberiZdravstveniKarton(ehrId, function (err, data) {
    if (err) {
      console.error(err);
      e.data.toastr.error(err.message, "Napaka");
      return;
    }
    $("#pk_space").append('<table class="table"><tbody><tr>\
      <td>Ime</td><td>' + data.firstNames + '</td></tr>\
      <tr><td>Priimek</td><td>' + data.lastNames + '</td></tr>\
      <tr><td>Datum rojstva</td><td>' + data.dateOfBirth + '</td></tr>\
      <tr><td>Spol</td><td>' + ((data.gender == "MALE")?"Moški":((data.gender == "FEMALE")?"Ženski":"")) + '</td></tr>\
      <tr><td>Lokacija</td><td>' + ((data.address)?data.address.address:"") + '</td></tr>\
      </tbody></table>');
    e.data.toastr.success("Podatki uspešno prebrani");
    
    // Dodaj na seznam predlogov pacientov, če še ni na seznamu
    let found = false;
    for(let i = 0; i < gPacienti.length; i++)
      if (gPacienti[i].ehrId == ehrId) {
        found = true;
        break;
      }
    if (!found) { // Predloga še ni na seznamu zato ga dodaj
      gPacienti.push({ ehrId: ehrId, name: data.firstNames + " " +  data.lastNames }); // Dodaj k predlogom
      posodobiPrikazSeznamaPacientov();
    }
  });
}

/**
 * Ko smo prejeli zahtevo za vnos meritev
 */
function onVnosMeritev(e) {
  let ehrId = $("#vm_ehrId").val();
  let merilec = $("#vm_merilec").val();
  let datumInUra = $("#vm_datumInUra").val();
  let drzava = $("#vm_drzavaCode").val();
  
   if (!ehrId || ehrId.trim().length == 0) {
    e.data.toastr.error("Vnesi ehrId", "Napaka");
    return;
  } else if (!merilec || merilec.trim().length == 0) {
    e.data.toastr.error("Vnesi podatke o merilcu", "Napaka");
    return;
  } else if (!datumInUra || datumInUra.trim().length == 0) {
    datumInUra = (new Date(Date.now())).toISOString().substr(0,19) + "Z"; // Če datum in ura nista podana, postavi na trenutno
  }  else if (!drzava || drzava.trim().length == 0) {
    e.data.toastr.error("Izberi državo", "Napaka");
    return;
  }
  
  ustvariMeritev(ehrId,drzava, datumInUra, merilec, {
    height: $("#vm_visina").val(),
    weight: $("#vm_teza").val(),
    temperature: $("#vm_temperatura").val(),
    systolic: $("#vm_sistolicni").val(),
    diastolic: $("#vm_diastolicni").val(),
    oximetry: $("#vm_kisk").val()
  }, function (err, data){
    if (err) {
      console.error(err);
      e.data.toastr.error(err.message, "Napaka");
      return;
    }
    e.data.toastr.success("Meritev vnešena");
    
    // Dodaj na seznam predlogov pacientov, če še ni na seznamu
    preberiZdravstveniKarton(ehrId, function (err, data) {
      if (err) {
        console.error(err);
        return;
      }
      let found = false;
      for(let i = 0; i < gPacienti.length; i++)
        if (gPacienti[i].ehrId == ehrId) {
          found = true;
          break;
        }
      if (!found) { // Predloga še ni na seznamu zato ga dodaj
        gPacienti.push({ ehrId: ehrId, name: data.firstNames + " " +  data.lastNames }); // Dodaj k predlogom
        posodobiPrikazSeznamaPacientov();
      }
    });
    
    
    // Počisti za seboj (Razen ehrId)
    $("#vm_merilec").val("");
    $("#vm_datumInUra").val("");
    $("#vm_visina").val("");
    $("#vm_teza").val("");
    $("#vm_temperatura").val("");
    $("#vm_sistolicni").val("");
    $("#vm_diastolicni").val("");
    $("#vm_kisk").val("");
  });
}

/**
 * Ko smo prejeli zahtevo za branje meritev
 */
function onPreberiMeritve(e) {
  $("#pm_space").html("");
  let ehrId = $("#pm_ehrId").val();
  let tip = $("#pm_seznamTipov").val();
  let from = $("#pm_from").val();
  let to = $("#pm_to").val();
  let limit = $("#pm_limit").val();
  let offset = $("#pm_offset").val();
  
  if (!ehrId || ehrId.trim().length == 0) {
    e.data.toastr.error("Vnesi ehrId", "Napaka");
    return;
  } else if (!tip || tip.trim().length == 0) {
    e.data.toastr.error("Izberi tip meritve", "Napaka");
    return;
  }
  
  preberiMeritve(ehrId, tip, {
    from: from,
    to: to,
    limit: limit,
    offset: offset
  }, function(err, data) {
    if (err) {
      console.error(err);
      e.data.toastr.error(err.message, "Napaka");
      return;
    }
    
    let table = '<table class="table"><thead><tr><th>Datum</th>';
    if (tip == "height") {
      table += '<th>Višina</th>';
    } else if (tip == "weight") {
      table += '<th>Teža</th>';
    } else if (tip == "temperature") {
      table += '<th>Temperatura</th>';
    } else if (tip == "pressure") {
      table += '<th>Diastolic</th>';
      table += '<th>Systolic</th>';
    } else if (tip == "spO2") {
      table += '<th>Nasičenost krvi</th>';
    } 
    table += '</tr></thead><tbody>';
    if (Array.isArray(data)) {
      for (let i = 0; i < data.length; i++) {
        table += '<tr><td>' + data[i].time + "</td>";
        if (tip == "height") {
          table += '<td>' + data[i].height + ' ' + data[i].unit + '</td>';
        } else if (tip == "weight") {
          table += '<td>' + data[i].weight + ' ' + data[i].unit + '</td>';
        } else if (tip == "temperature") {
          table += '<td>' + data[i].temperature + ' ' + data[i].unit + '</td>';
        } else if (tip == "pressure") {
          table += '<td>' + data[i].diastolic + ' ' + data[i].unit + '</td>';
          table += '<td>' + data[i].systolic + ' ' + data[i].unit + '</td>';
        } else if (tip == "spO2") {
          table += '<td>' + data[i].spO2 + ' %</td>';
        }
        table += '</tr>';
      }
    }
    table += '</tbody></table>';
    $("#pm_space").append(table);
    e.data.toastr.success("Podatki uspešno prebrani");
    
    preberiZdravstveniKarton(ehrId, function (err, data) {
      if (err) {
        console.error(err);
        return;
      }
      
      // Dodaj na seznam predlogov pacientov, če še ni na seznamu
      let found = false;
      for(let i = 0; i < gPacienti.length; i++)
        if (gPacienti[i].ehrId == ehrId) {
          found = true;
          break;
        }
      if (!found) { // Predloga še ni na seznamu zato ga dodaj
        gPacienti.push({ ehrId: ehrId, name: data.firstNames + " " +  data.lastNames }); // Dodaj k predlogom
        posodobiPrikazSeznamaPacientov();
      }
    });
  });
}

/**
 * Ko smo prejeli zahtevo za pregled podatke o pacientu
 */
function onPrikaziPodatkePacienta(e) {
  let ehrId = $(e.target).attr("ehrId");
  gIzbraniEHRId = ehrId;
  if ($("#navigacija li[optid=2]").hasClass("disabled"))
    $("#navigacija li[optid=2]").removeClass("disabled"); // Odstrani disabled class
  spremeniZavihek(2); // Prikaži 2 zavihek
}

/**
 * Ko smo prejeli zahtevo za vnos podatkov o pacientu
 */
function onUrediPodatkePacienta(e) {
  let ehrId = $(e.target).attr("ehrId");
  gIzbraniEHRId = ehrId;
  spremeniZavihek(3);
}

/**
 * Ko smo prejeli EHR ID od pacienta, od katerega podatke moramo prikazati
 */
function onRocniPrikazPacienta(e) {
  let ehrId = $("#ip_ehrId").val();
  if (!ehrId || ehrId.trim().length == 0) {
    console.error("Vnesite ehrId");
    e.data.toastr.error("Vnesite ehrId", "Napaka");
    return;
  }
  gIzbraniEHRId = ehrId;
  if ($("#navigacija li[optid=2]").hasClass("disabled"))
    $("#navigacija li[optid=2]").removeClass("disabled"); // Odstrani disabled class
  spremeniZavihek(2); // Prikaži 2 zavihek
}

/*
  Funkcionalnosti povezani z mapo
*/

/**
 * Popravi vrstni red koordinat, če sta slučajno zamenjani
 * @parm koordinate Tabela koordinat
 * @return Tabela popravljenih koordinat
 */
function popraviKoordinate(koordinate) {
  if (koordinate == null)
    return null;
  if (!Array.isArray(koordinate) || koordinate.length != 2)
    return koordinate;
  
  if (koordinate[0] < koordinate[1])
    return [koordinate[1], koordinate[0]];
  return koordinate;
}

/**
 * Popravi več koordinat
 * @param koordinate Tabela parov koordinat
 * @return Popravljena tabela
 */
function popraviVecKoordinat(koordinate) {
  if (koordinate == null)
    return null;
  if (!Array.isArray(koordinate) || koordinate.length == 0) {
    //console.log("JJJ");
    return koordinate;
  } 
    
  //console.log("ABBA");
  let res = [];
  for (let i = 0; i < koordinate.length; i++) {
    if (Array.isArray(koordinate[i])) { // Pogledamo, če podana tabela vsebuje podtabele
      if (koordinate[i].length == 2 && !Array.isArray(koordinate[i][0])) { // v notranji tabeli imamo koordinate zato menjamo
        //console.log("Reverse: " + koordinate[i]);
        res.push(popraviKoordinate(koordinate[i]));
      } else { // Moramo iti se stopnjo nizje
        //console.log("ABII");
        res.push(popraviVecKoordinat(koordinate[i]));
      }
    } else { // Ze podana tabela vsebuje koordinate
      //consol.log("CCC");
      return popraviKoordinate(koordinate);
    }
  }
  return res;
}

/**
 * Doda oznako na zemljevid
 * @param feature Marker
 */
function mapaDodajMarkerGeoJson(feature) {
  if (feature == null || mapa == null || feature.type != "Feature")
    return;
  
  let marker = null;
  let markerPopupMessage = "";
  
  if (feature.geometry.type == "Polygon" || feature.geometry.type == "LineString") { // Označi celotno stavbo
    feature.geometry.coordinates = popraviVecKoordinat(feature.geometry.coordinates);
    marker = L.polygon(feature.geometry.coordinates, {color: 'blue'});
  } else if (feature.geometry.type == "Point") { // Označi točko
    var ikona = new L.Icon({
      iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-blue.png',
      shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
    feature.geometry.coordinates = popraviKoordinate(feature.geometry.coordinates);
    marker = L.marker(feature.geometry.coordinates,  {icon: ikona});
  }
  
  if (marker != null) {
    // Izpišemo podatke v obločku
    let data = feature.properties;
    if (!feature.properties["name"] && feature.properties["@relations"] != null && Array.isArray(feature.properties["@relations"]) && feature.properties["@relations"].length > 0) { // Za povezane stavbe
      data = feature.properties["@relations"][0].reltags;
    }
    
    if (data["name"]) { // Dodaj ime markerja oblačku
      markerPopupMessage += data["name"] + "<br>"
    }
    if (data["addr:street"] && data["addr:housenumber"]) { // Dodaj naslov markerja oblačku
      markerPopupMessage += data["addr:street"] + " " + data["addr:housenumber"] + ", ";
    }
    if (data["addr:city"]) {
      markerPopupMessage += data["addr:city"];
    }
    
    // Dodamo točko na zemljevid in v seznam
    
    marker.addTo(mapa);
    markerji.push({
      ref: marker,
      data: feature
    });
    /*if (data["name"] == "Univerzitetni klinični center Ljubljana")
      marker.setStyle({color: "green"});*/
    
    // Dodamo oblačev označenim elementom (mora biti za addTo() !!!)
    if (markerPopupMessage != null && markerPopupMessage.length > 0) {
      marker.bindPopup("<div>" + markerPopupMessage + "</div>").openPopup();
    }
  }
}

/**
 * Funkcija, ki se kliče ob kliku na neko točko na zemljevidu
 * @param e 
 */
function onMapClick(e) {
  let latLng = e.latlng; // Pridobi informacije o tock, na katero smo kliknili na zemljevidu
  if (latLng == null) { // Če ni podatko o izbrani toči ne spreminjaj
    console.error("Can't get coordinates");
    return;
  }
  
  // Pobriši pot do prejšnje točke
  if (mapRouter != null) {
    mapRouter.removeFrom(mapa);
  }
  
  // Pobriši prejšnjo izbrano točko
  if (izbranaTocka != null) {
    izbranaTocka.removeFrom(mapa); // Izbriši prejšnjo izbrano točko
  }
  
  var ikona = new L.Icon({
      iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-red.png',
      shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  izbranaTocka = L.marker(latLng,  {icon: ikona});
  izbranaTocka.addTo(mapa);
  
  // Pobarvaj najbližjo bolnišnico
  let nearestHospital = null;
  let nearestDistance = 0;
  let nearestEdge = 0;
  
  for (let i = 0; i < markerji.length; i++) {
    // Preskoci tocke
    if (markerji[i].data.geometry.type == "Point")
      continue;
    
    // Primerjaj vse robne točke objektov
    let razdalja = 0;
    for (let ci = 0; ci < markerji[i].data.geometry.coordinates[0].length; ci++) {
      markerji[i].ref.setStyle({color: "blue"}); // Sproti ostale pobarvaj na modro
      razdalja = distance(latLng.lat, latLng.lng, markerji[i].data.geometry.coordinates[0][ci][0],markerji[i].data.geometry.coordinates[0][ci][1], "K");
      if (nearestHospital == null ||  razdalja < nearestDistance) {
        nearestHospital = markerji[i];
        nearestDistance = razdalja;
        nearestEdge = ci;
      }
    }
  }
  if (nearestHospital != null) {
    nearestHospital.ref.setStyle({color: "green"}); // Obarvamo najbližjo bolnišnico zeleno
    
    // Prikaži pot do bolnišnice
    // GPS zahteva HTTPS,
    /*mapRouter = L.Routing.control({
      waypoints: [
        L.latLng(latLng.lat,latLng.lng),
        L.latLng(nearestHospital.data.geometry.coordinates[0][nearestEdge][0],nearestHospital.data.geometry.coordinates[0][nearestEdge][1])
      ],
      language: 'sl',
      showAlternatives: true
    });
    mapRouter.addTo(mapa);*/
    
    // Pripravi sporočilo o najbljižji bolnišnici
    let markerPopupMessage = "";
    if (nearestHospital.data.properties["name"]) { // Dodaj ime markerja oblačku
      markerPopupMessage += nearestHospital.data.properties["name"] + "<br>"
    }
    if (nearestHospital.data.properties["addr:street"] && nearestHospital.data.properties["addr:housenumber"]) { // Dodaj naslov markerja oblačku
      markerPopupMessage += nearestHospital.data.properties["addr:street"] + " " + nearestHospital.data.properties["addr:housenumber"] + ", ";
    }
    if (nearestHospital.data.properties["addr:city"]) {
      markerPopupMessage += nearestHospital.data.properties["addr:city"];
    }
    toastr.info(markerPopupMessage, "Najbližja bolnišnica", {timeOut: 10000}); // Izpiši sporočilo uporabniku do najbližje bolnišnice
  }
}

/**
 * Pridobi lokacijo
 * @see https://www.w3schools.com/html/html5_geolocation.asp
 * 
 * @param callback Povratni klic
 */
function geolocation(callback) {
  if (navigator.geolocation) { // Pridobi geolokacijo
    navigator.geolocation.getCurrentPosition(callback, function(error){
      let message = "";
      switch(error.code) {
        case error.PERMISSION_DENIED:
          message = "User denied the request for Geolocation."
          break;
        case error.POSITION_UNAVAILABLE:
          message = "Location information is unavailable."
          break;
        case error.TIMEOUT:
          message = "The request to get user location timed out."
          break;
        case error.UNKNOWN_ERROR:
          message = "An unknown error occurred."
          break;
      }
      console.error(message);
      toastr.error(message, 'Napaka', {timeOut: 5000});
      callback(null);
    });
  } else {
    console.error("Napaka pri geolokaciji");
    toastr.error('Ni mogoče pridobiti lokacije', 'Napaka', {timeOut: 5000});
    callback(null);
  }
}

/*
  Pridobitev podatkov iz zunanjih virov
*/

/**
 * Pridobi seznam bolnišnic
 * @param callback Povratini klic, ki ima argument s seznamom bolnišnic ali null v primeru napake
 */
function pridobiSeznamBolnisnic(callback) {
  $.ajax({
    url: "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json",
    type: 'GET',
    success: function(res) {
      if (res != null) {
        callback(res.features);
      } else {
        callback(null);
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.error(errorThrown);
      callback(null);
    }
  });
}

/**
 * Ustvari EHR zapis za osebo in dodaj demografske podatke o osebi
 * @param oseba Podatki o osebi {firstNames,lastNames,dateOfBirth,gender,...}
 * @param callback (error, ehrId)
 */
function ustvariZdravstveniKarton(oseba, callback) {
  // Preveri zahtevane podatke
  if (!oseba || !oseba.firstNames || !oseba.lastNames || !oseba.dateOfBirth) {
		if (callback) return callback({ id: 1, message: "Prosim vnesite manjkajoce podatke" }, null);
		return;
	}
	
	// Ustvari karton
	$.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (data) {
      // Pripravi podatke
      var ehrId = data.ehrId;
      var partyData = oseba;
      oseba.additionalInfo = { 
        "ehrId": ehrId 
      };
      
      // Ustvari demografske podatke
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function (party) {
          if (party.action == 'CREATE') {
            if (callback) return callback(null, ehrId);
          } else {
            if (callback) return callback({ id: 2, message: "" }, null);
          }
        },
        error: function(err) {
          if (callback) return callback({ id: 3, message: JSON.parse(err.responseText).userMessage }, null);
        }
      });
    }
	});
}

/**
 * Preberi EHR zapis za osebo
 */
function preberiZdravstveniKarton(ehrId, callback) {
  if (!ehrId || ehrId.trim().length == 0) {
    if (callback) return callback({ id: 4, message: "Neveljaven ehrId" });
    return;
  }
  
  // Pridobi podatke o kartonu iz strežnika
	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		type: 'GET',
		headers: {
      "Authorization": getAuthorization()
    },
  	success: function (data) {
			if (callback) return callback(null, data.party);
		},
		error: function(err) {
			if (callback) return callback({ id: 5, message: JSON.parse(err.responseText).userMessage }, null);
		}
	});
}

/**
 * Zapiše meritve
 * @param ehrId ID zdravstvenega kartona
 * @param drzava 2 črkovna kratica države
 * @param datumInUra Datum in ura
 * @param merilec Oseba, ki je izmerila podatke
 * @param meritve Objekt z informacijami o samih meritvah
 * @param callback (error, data)
 */
function ustvariMeritev(ehrId, drzava, datumInUra, merilec, meritve, callback) {
  // Prveri zahtevane podatke
	if (!ehrId || ehrId.trim().length == 0) {
		if (callback) return callback({ id: 6, message: "Prosim vnesite manjkajoce podatke" }, null);
		return;
	}
	// Pripravi podatke
	var podatki = {
		// Struktura predloge je na voljo na naslednjem spletnem naslovu:
    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
    "ctx/language": "en",
    "ctx/territory": drzava,
    "ctx/time": datumInUra,
    "vital_signs/height_length/any_event/body_height_length": meritve.height,
    "vital_signs/body_weight/any_event/body_weight": meritve.weight,
   	"vital_signs/body_temperature/any_event/temperature|magnitude": meritve.temperature,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": meritve.systolic,
    "vital_signs/blood_pressure/any_event/diastolic": meritve.diastolic,
    "vital_signs/indirect_oximetry:0/spo2|numerator": meritve.oximetry
	};
	var parameters = {
	    ehrId: ehrId,
	    templateId: 'Vital Signs',
	    format: 'FLAT',
	    committer: merilec
	};
	$.ajax({
    url: baseUrl + "/composition?" + $.param(parameters),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(podatki),
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
      if (callback) return callback(null, null);
    },
    error: function(err) {
    	if (callback) return callback({ id: 6, message: JSON.parse(err.responseText).userMessage }, null);
    }
	});
}

/**
 * Preberi meritve za posamezen tip
 * @param ehrId ID zdravstvenega kartona
 * @param tip Tip meritve
 * @param callback (error,data)
 */
function preberiMeritve(ehrId, tip, queryData, callback) {
  let tipMap = {
	  temperature: "body_temperature",
	  blood_pressure: "blood_pressure",
	  pressure: "blood_pressure",
	  systolic: "blood_pressure",
	  diastolic: "blood_pressure",
	  height: "height",
	  spO2: "spO2",
	  weight: "weight",
	};
	
  // Preveri zahtevane podatke
	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0 || !tipMap[tip]) {
		if (callback) return callback({ id: 7, message: "Vnesi manjkajoce podatke" }, null);
	  return;
	}
	
  $.ajax({
	  url: baseUrl + "/view/" + ehrId + "/" + tipMap[tip],
    type: 'GET',
    data: queryData,
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (res) {
    	 if (callback) return callback(null, res);
    },
    error: function(err) {
      if (callback) return callback({ id: 8, message: JSON.parse(err.responseText).userMessage }, null);
    }
	});

}

/**
 * Pridobi seznam držav
 * Vir: https://download.geonames.org/export/dump/countryInfo.txt
 * Pretvorba v JSON: https://www.csvjson.com/csv2json
 */
function pridobiSeznamDrzav(callback) {
  $.ajax({
	  url: "knjiznice/json/drzave.json",
    type: 'GET',
    success: function (res) {
    	if (callback) return callback(null, res);
    },
    error: function(err) {
      if (callback) return callback({ id: 9, message: JSON.stringify(err) }, null);
    }
	});
}

/**
 * Pridobi seznam slovenskih krajev
 * Vir: https://api.lavbic.net/kraji
 */
function pridobiSeznamKrajev(callback) {
   $.ajax({
	  url: "https://api.lavbic.net/kraji",
    type: 'GET',
    success: function (res) {
    	if (callback) return callback(null, res);
    },
    error: function(err) {
      if (callback) return callback({ id: 9, message: JSON.stringify(err) }, null);
    }
	});
}

/*
################################################################################
*/

/**
 * Ko se spletna stran naloži se izvedejo spodnje funkcije
 */
window.addEventListener('load', function () {
  /**
   * Če smo na strani s zemljevidom izvedemo stvari, ki so potrebne za prikaz zemljevida
   */
  if ($("#zemljevid").length) { // Če obstaja polje namenjeno zemljevidu
    
    // Osnovne lastnosti mape
    var mapOptions = {
      center: [FRI_LAT, FRI_LNG],
      zoom: 12
    };

    // Ustvarimo objekt mapa
    mapa = new L.map("zemljevid", mapOptions);
  
    // Ustvarimo prikazni sloj mape
    var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  
    // Prikazni sloj dodamo na mapo
    mapa.addLayer(layer);
    
    // Pocentriraj mapo na FRI
    mapa.setView({lat: FRI_LAT, lng: FRI_LNG},mapa.getZoom());
    
    // Prenesemo seznam bolnišnic
    pridobiSeznamBolnisnic(function (data) {
      // Izrišemo objekte na zemljevidu
      for (let i = 0; i < data.length; i++) {
        mapaDodajMarkerGeoJson(data[i]);
      }
      
      // Nastavi trenutno lokacijo
      geolocation(function(position) {
        if (position) {
          onMapClick({latlng: {lat: position.coords.latitude, lng: position.coords.longitude}}); // Sproži risanje trenutne lokacije
      
          // Pocentriraj mapo na trenutno lokacijo
          mapa.setView({lat: position.coords.latitude, lng: position.coords.longitude},(mapa.getZoom() < 15)?15:mapa.getZoom());
        }
        
        // Dodaj poslušalec za klik na mapo
        mapa.on('click', onMapClick, this);
      });
    });
  }
  
  /**
   *  Če nalagamo glavno stran
  */
  if ($("#navigacija").length) {
    // Dodaj poslušalca za klik na nav
    $("#navigacija li").on("click", onNavbarLinkClick);
    
    spremeniZavihek(1); // Samo, da naložimo vse stvari za 1 zavihek
  }
  if($("#uk_submit").length) {
    $("#uk_submit").on("click", this, onUstvariZravstveniKarton);
  }
  if($("#pk_seznamPacientov").length) {
    $("#pk_seznamPacientov").on('change', function() {
      $("#pk_ehrId").val(this.value);
    });
  }
  if ($("#pk_submit").length) {
    $("#pk_submit").on("click", this, onPreberiZravstveniKarton);
  }
  if($("#vm_seznamPacientov").length) {
    $("#vm_seznamPacientov").on('change', function() {
      $("#vm_ehrId").val(this.value);
    });
  }
  if ($("#vm_submit").length) {
    $("#vm_submit").on("click", this, onVnosMeritev);
  }
  if($("#pm_seznamPacientov").length) {
    $("#pm_seznamPacientov").on('change', function() {
      $("#pm_ehrId").val(this.value);
    });
  }
  if ($("#pm_submit").length) {
    $("#pm_submit").on("click", this, onPreberiMeritve);
  }
  if ($("#ip_submit").length) {
    $("#ip_submit").on("click", this, onRocniPrikazPacienta);
  }
  if ($("#sw_seznamTipov").length) {
    $("#sw_seznamTipov").on("change", function(){
      prikaziPodatkePacienta();
    });
  }
  if ($("#sw_steviloMeritev").length) {
    $("#sw_steviloMeritev").on("change", function(){
      prikaziPodatkePacienta();
    });
  }
  if ($("#btn_generirajPodatke").length) {
    $("#btn_generirajPodatke").on("click", this, function() {
        for (let i = 1; i < 4; i++)
          generirajPodatke(i);
    });
  }
});